package app.com.browsermodule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private BrowserModule browserModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        browserModule = new BrowserModule(this);

        test();
    }

    public void test(){
        if(BrowserTests.test(browserModule)){
            Log.d("MAIN_ACTIVITY", "WELL DONE");
        }else{
            Log.d("MAIN_ACTIVITY", "SOME ERRORS");
        }
    }
}
