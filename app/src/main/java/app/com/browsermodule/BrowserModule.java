package app.com.browsermodule;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

public class BrowserModule{

    /**
     * <p>События генерируемые модулем</p>
     */
    private static final String ERROR_EVENT = "";
    private static final String START_EVENT = "";
    private static final String STOP_EVENT = "";
    private static final String EXIT_EVENT = "";

    //внутренний браузер
    private Browser browser;

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "BROWSER_MODULE";

    private Activity activity;

    public BrowserModule(Activity _activity){
        activity = _activity;
        browser = new Browser(Browser.DIALOG);
    }

    /**
     * <p>Метод, который вызоветься при Application.onPause()</p>
     */

    public void pause() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onCreate()</p>
     */

    public void create() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onResume()</p>
     */

    public void resume() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onDestroy()</p>
     */

    public void destroy() {

    }

    /**
     * <p>Метод, который вызоветься при Application.onActivityResult()</p>
     */
    public void activityResult(int requestCode, int resultCode, Intent data){

    }

    /**
     * Создает браузер для работы с веб вью и веб контентом
     */
    public static class Browser{

        private WebView inAppWebView;
        private Dialog dialog;
        private boolean show;

        private int currentType;
        public static final int DIALOG = 1;
        public static final int LAYOUT = 2;

        public Browser(int type){
            currentType = type;
        }

        /**
         * Инициализирует webview браузер
         * @param show показывать ли изначально браузер
         */
        public void init(boolean show){

        }

        /**
         * Вставляет ксс на текущю страницу
         * @param css строка ксс для вставки
         */
        public void insertCSS(String css){

        }

        /**
         * закрывает диалог
         */
        public void close(){

        }

        /**
         * Показывает диалог
         */
        public void show(){

        }

        /**
         * Вставляет и исполняет джаваскрипт на текущю страницу
         * @param js строка ксс для вставки
         */
        public void executeScript(String js){

        }

        /**
         * Загрузка страницы
         * @param url
         */
        public void loadURL(String url){

        }

        /**
         * Для вставки ксс и джаваскрипта
         * @param source
         * @param jsWrapper
         * @param isJs
         */
        private void injectDeferredObject(String source, String jsWrapper, boolean isJs){

        }

    }

    class MyWebClient{
        private View myCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        private FrameLayout mFullscreenContainer;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        public MyWebClient(){

        }

        public Bitmap getDefaultVideoPoster(){
            return null;
        }

        public void onHideCustomView(){

        }

        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback){

        }
    }

    class JavaScriptHandler{
        public JavaScriptHandler(Context context){

        }

        public void callback(String cib, String res){

        }
    }

    class InAppBrowserClient{
        public InAppBrowserClient(){

        }

        public void onPageStarted(WebView view, String url, Bitmap favicon){
            //generate event
        }

        public void onPageFinished(WebView view, String url){
            //generate event
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl){
            //generate event
        }
    }
}
